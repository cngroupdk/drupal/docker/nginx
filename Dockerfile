FROM nginx:1.17

WORKDIR /var/www/html

COPY vhost.conf /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/nginx.conf
RUN chmod -R 777 /var/cache \
    && chmod -R 777 /var/run

EXPOSE 80
